package ru.arubtsova.tm.exception.system;

import ru.arubtsova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(final String argument) {
        super("Error! Unknown argument ``" + argument + "``...");
    }

}
