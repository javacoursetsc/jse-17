package ru.arubtsova.tm.command.project;

import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String description() {
        return "update a project by id.";
    }

    @Override
    public void execute() {
        System.out.println("Project:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter New Project Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Project Description:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = serviceLocator.getProjectService().updateProjectById(id, name, description);
        if (projectUpdate == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully updated");
    }

}
