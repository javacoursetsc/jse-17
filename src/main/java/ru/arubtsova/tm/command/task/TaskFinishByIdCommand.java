package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String description() {
        return "change task status to Complete by task id.";
    }

    @Override
    public void execute() {
        System.out.println("Task:");
        System.out.println("Enter Task Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

}
