package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.enumerated.Status;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-change-status-by-name";
    }

    @Override
    public String description() {
        return "change task status by task name.";
    }

    @Override
    public void execute() {
        System.out.println("Task:");
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = serviceLocator.getTaskService().findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        final Task taskStatusUpdate = serviceLocator.getTaskService().changeTaskStatusByName(name, status);
        if (taskStatusUpdate == null) throw new TaskNotFoundException();
        System.out.println("Task status was successfully updated");
    }

}
